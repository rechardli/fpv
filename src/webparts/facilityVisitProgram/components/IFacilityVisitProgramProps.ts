import {WebPartContext} from '@microsoft/sp-webpart-base';
export interface IFacilityVisitProgramProps {
  description: string;
  context:WebPartContext;
}
