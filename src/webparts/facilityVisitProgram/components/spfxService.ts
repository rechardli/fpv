import '@pnp/polyfill-ie11';
import {sp} from '@pnp/sp/presets/all';
import {WebPartContext} from '@microsoft/sp-webpart-base';
import {PageContext} from '@microsoft/sp-page-context';
import {IItemAddResult} from '@pnp/sp/items';

export interface IListItem{
    Title:string;
    Region:string;
}

export class SpfxService{
    constructor(context:WebPartContext, myPageContext:PageContext){
        sp.setup({
            spfxContext:context,
            sp:{
                headers:{
                    "Accept":"application/json; odata=verbose"
                }
            },
            ie11:true
        });
    }

    public async getAllrecords(listname:string):Promise<IListItem[]>{
        const result:IListItem[]=[];
        return new Promise<IListItem[]>(async(resolve, reject)=>{
            sp.web.lists.getByTitle(listname).items.getAll().then((items) => {
                items.map((item) => {
                    result.push({Title:item.Title, Region:item.Region});
                });
                resolve(result);
            });

        });
    }
    public async addRecords(listname:string, title:string, region:string):Promise<IItemAddResult>{
        return new Promise<IItemAddResult>(async(resolve, reject) =>{
            const result = sp.web.lists.getByTitle(listname).items.add({
                Title:title,
                Region:region,
            });
            resolve(result);
        });
    }
}