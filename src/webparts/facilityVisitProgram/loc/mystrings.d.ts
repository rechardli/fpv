declare interface IFacilityVisitProgramWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'FacilityVisitProgramWebPartStrings' {
  const strings: IFacilityVisitProgramWebPartStrings;
  export = strings;
}
